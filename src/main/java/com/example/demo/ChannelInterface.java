package com.example.demo;
import com.example.demo.model.Channel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChannelInterface extends JpaRepository<Channel,Long>{
}
