package com.example.demo.model;


import java.util.Objects;

public class TVProgram {
    private String seriesName;
    private int popularityScore;

    public TVProgram() {
    }

    public TVProgram(String seriesName, int popularityScore) {
        this.seriesName = seriesName;
        this.popularityScore = popularityScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TVProgram tvProgram = (TVProgram) o;

        System.out.println(tvProgram.popularityScore+" "+tvProgram.seriesName);

        return popularityScore == tvProgram.popularityScore && Objects.equals(seriesName, tvProgram.seriesName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(seriesName, popularityScore);
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public int getPopularityScore() {
        return popularityScore;
    }

    public void setPopularityScore(int popularityScore) {
        this.popularityScore = popularityScore;
    }
}
