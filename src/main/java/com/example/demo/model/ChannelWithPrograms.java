package com.example.demo.model;

import java.util.List;
import java.util.Objects;

public class ChannelWithPrograms {

    private String channelName;
    private int rating;
    private List<TVProgram> programs;

    public ChannelWithPrograms(String channelName, int rating, List<TVProgram> tvPrograms) {
        this.channelName = channelName;
        this.rating = rating;
        this.programs = tvPrograms;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChannelWithPrograms that = (ChannelWithPrograms) o;

        System.out.println(that.channelName+" "+that.rating);

        return rating == that.rating && Objects.equals(channelName,that.channelName)
                && Objects.equals(programs, that.programs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(channelName, rating, programs);
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<TVProgram> getPrograms() {
        return programs;
    }

    public void setPrograms(List<TVProgram> programs) {
        this.programs = programs;
    }


}
