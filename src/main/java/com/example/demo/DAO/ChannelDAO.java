package com.example.demo.DAO;

import com.example.demo.model.Channel;
import com.example.demo.model.ChannelWithPrograms;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.example.demo.DAO.ChannelWithProgramsDAO.channelWithProgramsList;

@Service
public class ChannelDAO {

    public Map<String, List<Channel>> getAllChannels() {
        ChannelWithProgramsDAO controller = new ChannelWithProgramsDAO();
        List<Channel> channelList = new ArrayList<>();

        for (ChannelWithPrograms channel : channelWithProgramsList) {
            channelList.add(new Channel(channel.getChannelName(), channel.getRating()));
        }

        HashMap<String, List<Channel>> map = new HashMap<>();
        map.put("items", channelList);
        return map;
    }
}
