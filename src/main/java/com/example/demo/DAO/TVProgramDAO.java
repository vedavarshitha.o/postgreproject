package com.example.demo.DAO;

import com.example.demo.model.ChannelWithPrograms;
import com.example.demo.model.TVProgram;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.example.demo.DAO.ChannelWithProgramsDAO.channelWithProgramsList;

@Service
public class TVProgramDAO {

    public Map<String, List<TVProgram>> getAllPrograms() {
        List<TVProgram> tvProgramList = new ArrayList<>();

        for (ChannelWithPrograms tvProgram : channelWithProgramsList) {
            tvProgramList.addAll(tvProgram.getPrograms());
        }

        HashMap<String, List<TVProgram>> map = new HashMap<>();
        map.put("items", tvProgramList);
        return map;
    }
}
