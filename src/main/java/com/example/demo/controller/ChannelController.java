package com.example.demo.controller;

import com.example.demo.ChannelInterface;
import com.example.demo.model.Channel;
import com.example.demo.DAO.ChannelDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class ChannelController {
    @Autowired
    private ChannelInterface channelInterface;

    @RequestMapping(path = "/channels")
    public Map<String, List<Channel>> getChannels() {
        return (Map<String, List<Channel>>) channelInterface.findAll();
    }

    @PostMapping("/employees")
    public Channel createChannel(@Valid @RequestBody Channel channel) {
        //logger.info("Insert employee...");
        return channelInterface.save(channel);
    }
}
