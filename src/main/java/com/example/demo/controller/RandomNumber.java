package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RandomNumber {

    @GetMapping("/number")
    public int generateRandomNumber() {
        int i = (int) (Math.random() * 10);
        return i;
    }
}