package com.example.demo;

import com.example.demo.controller.*;
import com.example.demo.model.Channel;
import com.example.demo.model.ChannelWithPrograms;
import com.example.demo.model.TVProgram;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DemoApplicationTests {

    @Test
    public void TestIfGreetingReturnsHelloWorld() {
        String expected = "Hello World!";
        HelloWorldController helloWorld = new HelloWorldController();

        String actual = helloWorld.greeting();

        assertEquals(expected, actual);
    }

    @Test
    public void testRandomNumberIsBetweenOneAndTen() {
        RandomNumber randomNumber = new RandomNumber();

        int actualNumber = randomNumber.generateRandomNumber();

        assertTrue(0 <= actualNumber);
        assertTrue(actualNumber <= 10);
    }

    @Autowired
    ChannelController channelsController;

    @Autowired
    TVProgramController programController;

    @Autowired
    ChannelWithProgramsController channelWithProgramController ;

    @Test
    void testToCheckIfAllChannelsAreReturned() {
        List<Channel> expectedChannels = new ArrayList<>(Arrays.asList(new Channel("Work",10),
                new Channel("Life",10)));

        List<Channel> actualChannels=channelsController.getChannels().get("items");

        assertArrayEquals(new List[]{expectedChannels}, new List[]{actualChannels});
    }

    @Test
    void testToCheckIfAllTvProgramsAreReturned() {
        List<TVProgram> expectedPrograms = new ArrayList<>(Arrays.asList(new TVProgram(
                        "The Key Tee Party",
                        4
                ),

                new TVProgram(
                        "The Lockdown",
                        1
                ),
                new TVProgram(
                        "The Project",
                        5
                ),

                new TVProgram(
                        "The Intern",
                        5
                ),

                new TVProgram(
                        "The Training",
                        1
                )));

        List<TVProgram> actualPrograms=programController.getTVPrograms().get("items");

        assertArrayEquals(new List[]{expectedPrograms}, new List[]{actualPrograms});
    }

    @Test
    void testToCheckIfAllChannelsAndTvProgramsAreReturned() {
        List<ChannelWithPrograms> expectedList = new ArrayList<>(Arrays.asList(
                new ChannelWithPrograms("Work",
                        10,
                        Arrays.asList(new TVProgram(
                                        "The Key Tee Party",
                                        4
                                ),

                                new TVProgram(
                                        "The Lockdown",
                                        1
                                ))
                ),
                new ChannelWithPrograms(
                        "Life",
                        10,
                        Arrays.asList(
                                new TVProgram(
                                        "The Project",
                                        5
                                ),

                                new TVProgram(
                                        "The Intern",
                                        5
                                ),

                                new TVProgram(
                                        "The Training",
                                        1
                                )
                        ))));

        List<ChannelWithPrograms> actualList = channelWithProgramController.getChannelWithPrograms().get("items");

        assertArrayEquals(new List[]{expectedList}, new List[]{actualList});
        
    }
}
